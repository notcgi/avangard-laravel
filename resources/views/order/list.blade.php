<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <script src="/js/jquery-3.5.1.min.js.css"></script>

    </head>
    <body>
    <label> Filter:
        <select name="filter">
            <option value="overdue">overdue</option>
            <option value="current">current</option>
            <option value="new">new</option>
            <option value="done">done</option>
        </select>
    </label>
    <table id="tbl-orders" style="width: 100%">
            <thead style="font-weight: bold">
            <tr>
                <td>id</td>
                <td>status</td>
                <td>client_email</td>
                <td>partner_id</td>
                <td>delivery_dt</td>
                <td>created_at</td>
                <td>updated_at</td>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    <div style="width: 100%;display: table;" id="pag-links"></div>
    </body>
<script>
    $(document).ready(function() {
        var page = window.location.hash.replace('#', '')??0;
        $(document).on('change', '[name="filter"]', function() {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page) {
                    return getOrdersByAPI(page);
                }
            }
            return getOrdersByAPI(1);
        });
        getOrdersByAPI(page);
    });

    function getOrdersByAPI(page) {
        $.get({
            url: '/api/order?page=' + page + '&filter='+$("[name='filter']").val(),
            dataType: 'json',
        }).done(function(data) {
            var tbl_orders = $('#tbl-orders tbody');
            tbl_orders.empty();
            $(data.data).each(function (loop,order) {
                tbl_orders.append('<tr>' +
                    '<td>'+order.id+'</td>' +
                    '<td>'+order.status+'</td>' +
                    '<td>'+order.client_email+'</td>' +
                    '<td>'+order.partner_id+'</td>' +
                    '<td>'+order.delivery_dt+'</td>' +
                    '<td>'+order.created_at+'</td>' +
                    '<td>'+order.updated_at+'</td>' +
                    '</tr>')
            })
            $('#pag-links').html('<tr>' +
                '<td><a href="'+data.first_page_url+'">First page</a> </td>' +
                '<td><a href="'+data.prev_page_url+'">Prev page</a> </td>' +
                '<td>'+data.from+'-'+data.to +'/'+data.total+'</td>' +
                '<td><a href="'+data.next_page_url+'">Next page</a> </td>' +
                '<td><a href="'+data.last_page_url+'">Last page</a> </td>' +
                '</tr>')
            $(document).on('click', '#pag-links a', function(e) {
                e.preventDefault();
                getOrdersByAPI($(this).attr('href').split('page=')[1]);
            });
            location.hash = page;
        }).fail(function() {
            alert('Posts could not be loaded.');
        });
    }
</script>
</html>
