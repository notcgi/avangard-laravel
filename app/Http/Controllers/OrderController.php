<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

class OrderController extends BaseController
{
    use ValidatesRequests;

    /**
     * List of orders with filter and pagination
     * @return LengthAwarePaginator Order[]
     * @throws \Throwable
     */
    public static function list() : LengthAwarePaginator
    {
        // check type and set filter
        throw_if (!in_array(
            $filter = request('filter','overdue'),
            ['overdue','current','new','done']),
            new \Exception('Wrong type value')
        );
        // filter is setting by model Scope
        return $orders = Order::{$filter}()
            ->paginate($filter=='current'?15:50);
    }

    /**
     * show edit page
     *
     * @param Order $order
     * @return View
     */
    final public function edit(Order $order):View
    {
        return view('order/edit')->with('order',$order);
    }

    /**
     * save changes
     *
     * @param Order $order
     * @return RedirectResponse
     */
    final public function store(Order $order):RedirectResponse
    {
        $this->validate(request(), [
            'client_email' => 'required|email',
            'partner_id' => 'required|exists:partners,id',
            'status' => [
                'required',
                Rule::in(array_keys(Order::statuses)),
            ],
        ]);
        $order->fill(request()->all())->save();
        return redirect('/order');
    }
}